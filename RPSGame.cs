﻿
interface IRPSAi
{
    //Returns the player's choice in a game of rock paper scissors. 
    //Result should be "rock", "paper" or "scissors".
    string GetChoice(Player You, Player Opponent);

}



class Player
{
    public string Name{get;}
    public int Points {get; set;}
    public int RockCount {get; set;}
    public int PaperCount {get; set;}
    public int ScissorsCount {get; set;}
    public IRPSAi? Ai {get;}


    public void AddPoints(int add){
        this.Points = this.Points + add;
    }
    public void ChoseRock(){
        this.RockCount++;
    }
    public void ChosePaper(){
        this.PaperCount++;
    }
    public void ChoseScissors(){
        this.ScissorsCount++;
    }
    //Constructor takes the player's name, sets points and counts to 0.
    public Player(string Name, IRPSAi Ai){
        this.Name = Name;
        this.Points = 0;
        this.RockCount = 0;
        this.PaperCount = 0;
        this.ScissorsCount = 0;
        this.Ai = Ai;
    }
    //Copy Constructor takes a player and makes a copy of it, without including the AI.
     public Player(Player p){
        this.Name = p.Name;
        this.Points = p.Points;
        this.RockCount = p.RockCount;
        this.PaperCount = p.PaperCount;
        this.ScissorsCount = p.ScissorsCount;
        this.Ai = null;
    }   
}